console.log('Hello World');
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let userName = prompt("Enter Your Full Name: ");
	let userAge = prompt('Enter Your Age: ');
	let userAdd = prompt('Enter Your Address: ');
	console.log('Hello,' + userName + "!");
	console.log('You are ' + userAge + "years old.");
	console.log('You live in ' + userAdd + " City.");


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBands(){
		console.log('1. The Beatles');
		console.log('2. Metallica');
		console.log('3. The Eagles');
		console.log('4. L\'arc~en~Ciel');
		console.log('5. Eraserheads');
	};
	favBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favmovies (){
		let mRaiting = 97;
		console.log('1. The Godfather');
		console.log('Rotten Tomatoes Raiting: ' + mRaiting + '%');
		mRaiting = 96;
		console.log('2. The Godfather, Part II');
		console.log('Rotten Tomatoes Raiting: ' + mRaiting + '%');
		mRaiting = 91;
		console.log('3. Shawshank Redemtion');
		console.log('Rotten Tomatoes Raiting: ' + mRaiting + '%');
		mRaiting = 93;
		console.log('4. To Kill A Mockingbird');
		console.log('Rotten Tomatoes Raiting: ' + mRaiting + '%');
		mRaiting = 96;
		console.log('1. Psycho');
		console.log('Rotten Tomatoes Raiting: ' + mRaiting + '%');

	}
	favmovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printUsers = function (){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	
	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();

// console.log(friend1);
// console.log(friend2);